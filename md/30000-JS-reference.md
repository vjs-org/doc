## JS Reference

Here you can find documentation concerning nodes, javascript function etc. Every existing function can be extended like so:

#### Global objects:

| variable  | type | description |
|-----------|------|-------------|
| `graph`   | LGraph | the graph instance with 'alive' nodes |
| `graph.framedelay`   | LGraph | usually unset, but setting it to 1000 will limit graph-mainloop to 1 fps. Useful to save cpu. |
| `graphcanvas` | LGraphCanvas | renders a graph to a html canvas |
| `graphcanvas.canvas` | <canvas> | attached html canvas |
| `viewport` | domelement | a dom object which can be used for fullpage purposes (equals `document.querySelector('#viewport')`) |
| `_`        | object | contains utility functions like `_.wrap` (tip: add your own) |

#### Node events

| Event              | |
|--------------------|-|
|onAdded             | |
|onRemoved           | |
|onExecute           | |
|onAction            | |
|onConnectInput      | |
|onConnectionsChange | |
|onOutputDblClick    | |
|onOutputClick       | |
|onInputDblClick     | |
|onInputClick        | |
|onDblClick          | |
|onMouseDown         | |
|onMouseEnter        | |
|onMouseMove         | |
|onDropFile          | |
|onDropData          | |
|onDropItem          | |
|onSelected          | |
|onDeselected        | |
|onDrawForeground    | |
|onDrawCollapsed     | |
|onDrawBackground    | |
|onGetInputs         | |
|onGetOutputs        | |
|onStart             | |
|onStop              | |

Each node can be extended using javascript,  just create a script node and copy/paste this into the editor:

        // note: this runs every frame
        this.var = this.getInputDataByName("var", !this.var)
        this.setOutputData("var", this.var * 2 )

        this.onLoad  = () => {
          // this runs before nodes are added to the graph (pageload etc)
          require('./stdnodes.js')
          window.padstring = require('https://unpkg.com/padstring@1.0.0/padString.js')
        }

        this.onInit = () => {
            // this runs once a node is added to the graph
            // custom io instead of default inputs/outputs (rightclick node to enable)
            this.addInput("in2",LiteGraph.EVENT)
            this.addInput("var")
            this.addOutput("out2",LiteGraph.EVENT)
            this.addOutput("var")
        }

        this.onEvent = (action,args) => {
            // called when 'in2' or 'in' input is triggered
            this.trigger(false,{action,args}) // forward data to all (=false) outputs
        }

        return this.onEvent // optional: return promise for async support

The above demonstrates:

* custom inputs & outputs
* outputting data (`trigger`)
* using an npm module (`require')
* importing all the standard nodes (`stdnodes')

> NOTE 1: In case you want to modify global node-behaviour see the __Global modifications__ section.

> NOTE 2: This would overwrite functions for certain nodes, in such case wrapping is better:

        _.wrap(node, 'onMouseDown', function(onMouseDown){
            // do stuff before
            onMouseDown()
            // do stuff after
        })


#### Global modifications

In case eventlisteners are too limited, here's how you monkeypatch the whole editor / engine:

    // extend graph-function
    _.wrap( graph, 'createNode', (createNode, type, title, opts) => {
        // do stuff before
        createNode(type, title, opts)
        // do stuff later
    })

Same goes for nodes:

    // inspect node functions
    var nodes = LiteGraph.registered_node_types
    console.dir(nodes["graphics/image"])

    // extend node prototype function
    _.wrap( nodes["graphics/image"].prototype, 'collapse', (collapse) => {
        alert("don't collapse this one!")
        // collapse()
    })

> NOTE: the wrapping-technique above will affect all newly created nodes. To modify the behaviour of already created nodes, __Node events__ section above
