#### FAQ

* Q: how can i debug connections
* F: use the 'watch' or 'console' nodes (doubleclick on console-node)

* Q: how can i debug scripts
* F: add the console-node,  doubleclick it,  and use `console.log(myvar)` in your scripts

#### Contributing / Bugs

* Please report bugs [here](mailto:leonvankammen@gmail.com) (However, you are probably best off by extending/wrapping functions in vJS's javascript editor).
* Update documentation: please report [here](mailto:leonvankammen@mail.com) or do a pullrequest [here](https://gitlab.com/blokist/doc/tree/master/md) in case you're familiar with gitlab.
